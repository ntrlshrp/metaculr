## Test environments

* local R installation, R 3.6.3
* win-builder (devel)
* Windows Server 2022, R-devel, 64 bit
* Ubuntu Linux 20.04.1 LTS, R-release, GCC
* Fedora Linux, R-devel, clang, gfortran

## R CMD check results

0 errors | 0 warnings | 2 notes

* checking CRAN incoming feasibility ... NOTE
  Package has help file(s) containing install/render-stage \Sexpr{} expressions but no prebuilt PDF manual.
  
* checking for detritus in the temp directory ... NOTE
  Found the following files/directories:
    'lastMiKTeXException'
    
  This last NOTE shows up on Windows Server 2022, R-devel, 64 bit.
